package com.example.createimage;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class ImageActivity extends AppCompatActivity {

    private ConstraintLayout layoutImage;
    private Button btnShare;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);

        btnShare = findViewById(R.id.btn_share);
        layoutImage = findViewById(R.id.layout_image);

        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createImage();
            }
        });

    }

    private void createImage() {
        btnShare.setVisibility(View.GONE);
        Bitmap b = Bitmap.createBitmap(layoutImage.getWidth(), layoutImage.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        layoutImage.draw(c);

            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            b.compress(Bitmap.CompressFormat.JPEG, 40, bytes);

        File f = new File(Environment.getExternalStorageDirectory()
                + File.separator + "test.jpg");
        FileOutputStream fo = null;
        try {
            if (f.createNewFile()) {
                fo = new FileOutputStream(f);
                fo.write(bytes.toByteArray());

                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("image/jpeg");
                share.putExtra(Intent.EXTRA_STREAM, Uri.parse(f.getAbsolutePath()));
                startActivity(Intent.createChooser(share, "Share Image"));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fo != null) {
                try {
                    fo.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }
}
